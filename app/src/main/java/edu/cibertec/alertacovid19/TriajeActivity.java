package edu.cibertec.alertacovid19;

import androidx.appcompat.app.AppCompatActivity;
import edu.cibertec.alertacovid19.Entidad.Ciudadanos;
import edu.cibertec.alertacovid19.Entidad.Triaje;
import edu.cibertec.alertacovid19.Retrofit.Covid19Client;
import edu.cibertec.alertacovid19.Retrofit.Covid19Service;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

public class TriajeActivity extends AppCompatActivity {

    RadioButton rbQ1Si, rbQ1No, rbQ2Si, rbQ2No, rbQ3Si, rbQ3No, rbQ4Si, rbQ4No, rbQ5Si, rbQ5No;
    Button btnGrabarTriaje;
    String q1="No", q2="No", q3="No", q4="No", q5="No", nombre, nacionalidad, nroCel, tipoDocumento,nroDocumento;
    int id;

    Covid19Service covid19Service;
    Covid19Client covid19Client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_triaje);

        rbQ1Si = findViewById(R.id.rbQ1Si);
        rbQ1No = findViewById(R.id.rbQ1No);
        rbQ2Si = findViewById(R.id.rbQ2Si);
        rbQ2No = findViewById(R.id.rbQ2No);
        rbQ3Si = findViewById(R.id.rbQ3Si);
        rbQ3No = findViewById(R.id.rbQ3No);
        rbQ4Si = findViewById(R.id.rbQ4Si);
        rbQ4No = findViewById(R.id.rbQ4No);
        rbQ5Si = findViewById(R.id.rbQ5Si);
        rbQ5No = findViewById(R.id.rbQ5No);
        btnGrabarTriaje = findViewById(R.id.btnGrabarTriaje);

        id=getIntent().getExtras().getInt("id");
        nombre=getIntent().getExtras().getString("nombre");
        nacionalidad=getIntent().getExtras().getString("nacionalidad");
        nroCel=getIntent().getExtras().getString("nroCelular");
        tipoDocumento=getIntent().getExtras().getString("tipoDoc");
        nroDocumento=getIntent().getExtras().getString("nroDoc");

        covid19Client= Covid19Client.getInstance();
        covid19Service=covid19Client.getCovid19Service();

    }

    public void onBtnClick(View view) {

        Ciudadanos ciu = new Ciudadanos();
        ciu.setIdCiudadanos(id);
        ciu.setNombres(nombre);
        ciu.setNacionalidad(nacionalidad);
        ciu.setCelular(nroCel);
        ciu.setTipoDocumento(tipoDocumento);
        ciu.setNumeroDocumento(nroDocumento);

        Triaje obj = new Triaje();
        obj.setPregunta1(q1);
        obj.setPregunta2(q2);
        obj.setPregunta3(q3);
        obj.setPregunta4(q4);
        obj.setPregunta5(q5);
        obj.setCiudadanos(ciu);

        Call<Integer> call2 = covid19Service.doRegistrarTriaje(obj);
        call2.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.isSuccessful()) {
                    //TODO: enviar mensaje Toast  se grabo
                    //Log.i("GRABO:", "SE GRABO");
                    Toast.makeText(getApplicationContext(), "Se grabo", Toast.LENGTH_SHORT).show();
                    //Toast.makeText(getApplicationContext(), "Se grabó", Toast.LENGTH_LONG).show();
                } else {
                    //TODO: enviar mensaje Toast  no se grabo
                    Toast.makeText(getApplicationContext(), "No Se grabó", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {

            }
        });

    }

    public void onChkBoxClick(View view) {
        switch (view.getId()) {
            case R.id.rbQ1Si:
                q1="SI";
                break;
            case R.id.rbQ1No:
                q1="NO";
                break;
            case R.id.rbQ2Si:
                q2="SI";
                break;
            case R.id.rbQ2No:
                q2="NO";
                break;
            case R.id.rbQ3Si:
                q3="SI";
                break;
            case R.id.rbQ3No:
                q3="NO";
                break;
            case R.id.rbQ4Si:
                q4="SI";
                break;
            case R.id.rbQ4No:
                q4="NO";
                break;
            case R.id.rbQ5Si:
                q5="SI";
                break;
            case R.id.rbQ5No:
                q5="NO";
                break;
          }


    }
}



