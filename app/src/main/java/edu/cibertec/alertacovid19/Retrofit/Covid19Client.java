package edu.cibertec.alertacovid19.Retrofit;

import java.security.PublicKey;

import edu.cibertec.alertacovid19.Comun.Constantes;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Covid19Client {

    private static Covid19Client instance=null;
    private Covid19Service covid19Service;
    private Retrofit retrofit;

    public Covid19Client() {
        retrofit = new Retrofit.Builder()
                .baseUrl(Constantes.API_COVID19_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        covid19Service= retrofit.create(Covid19Service.class);
    }
    //Patron singleton
    public static Covid19Client getInstance(){
        if (instance==null) {
            instance = new Covid19Client();
        }
        return instance;
}
    public  Covid19Service getCovid19Service(){
        return covid19Service;
    }

}
