package edu.cibertec.alertacovid19.Retrofit;

import edu.cibertec.alertacovid19.Entidad.Ciudadano;
import edu.cibertec.alertacovid19.Entidad.Ciudadanos;
import edu.cibertec.alertacovid19.Entidad.Triaje;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface Covid19Service {
    /*@POST("ciudadano/")
    Call<Integer> doRegistrarCiudadano(@Body Ciudadano ciu);
    */

    @POST("saveCiudadano/")
    Call<Ciudadanos> doRegistrarCiudadano(@Body Ciudadanos ciu);


    @POST("saveTriajeCiudadano/")
    Call<Integer> doRegistrarTriaje(@Body Triaje triaje);

}
