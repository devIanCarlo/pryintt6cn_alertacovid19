package edu.cibertec.alertacovid19.Entidad;

import java.io.Serializable;

public class Ciudadanos implements Serializable {

    private Integer idCiudadanos;
    private String nombres;
    private String celular;
    private String nacionalidad;
    private String tipoDocumento;
    private String numeroDocumento;

    public Integer getIdCiudadanos() {
        return idCiudadanos;
    }

    public void setIdCiudadanos(Integer idCiudadanos) {
        this.idCiudadanos = idCiudadanos;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }
}


