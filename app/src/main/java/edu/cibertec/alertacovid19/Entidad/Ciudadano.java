package edu.cibertec.alertacovid19.Entidad;

import java.io.Serializable;

public class Ciudadano implements Serializable {

    private Integer idCiudadano;
    private Integer idTipoDocumento;
    private String nroDocumento;
    private String nombres;
    private String apellidos;
    private String nroCelular;
    private String email;
    private String sexo;
    private String nomApeContacto;
    private String celContacto;
    private String ubigeo;

    public Integer getIdCiudadano() {
        return idCiudadano;
    }

    public void setIdCiudadano(Integer idCiudadano) {
        this.idCiudadano = idCiudadano;
    }

    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(Integer idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getNroCelular() {
        return nroCelular;
    }

    public void setNroCelular(String nroCelular) {
        this.nroCelular = nroCelular;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getNomApeContacto() {
        return nomApeContacto;
    }

    public void setNomApeContacto(String nomApeContacto) {
        this.nomApeContacto = nomApeContacto;
    }

    public String getCelContacto() {
        return celContacto;
    }

    public void setCelContacto(String celContacto) {
        this.celContacto = celContacto;
    }

    public String getUbigeo() {
        return ubigeo;
    }

    public void setUbigeo(String ubigeo) {
        this.ubigeo = ubigeo;
    }
}
