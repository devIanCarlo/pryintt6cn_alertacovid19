package edu.cibertec.alertacovid19.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import edu.cibertec.alertacovid19.Entidad.Ciudadanos;
import edu.cibertec.alertacovid19.R;
import edu.cibertec.alertacovid19.Retrofit.Covid19Client;
import edu.cibertec.alertacovid19.Retrofit.Covid19Service;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;

    EditText etxtNombres, etxtApellidos, etxtNroCel,etxtNroDocumento;
    Spinner spnSexo,spnNacionalidad,spnTipoDocumento;
    Button btnContinuar,btnAtras;

    Covid19Service covid19Service;
    Covid19Client covid19Client;
    Ciudadanos cius=null;


    View root;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
         root = inflater.inflate(R.layout.fragment_home, container, false);
       /* final TextView textView = root.findViewById(R.id.text_home);
        homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });

        */

        covid19Client= Covid19Client.getInstance();
        covid19Service=covid19Client.getCovid19Service();
        findView();
        listener();


        return root;
    }


    void findView(){
        etxtNombres=(EditText) root.findViewById(R.id.etxtNombres);
        //etxtApellidos=(EditText) findViewById(R.id.etxtApellidos);
        etxtNroCel=(EditText) root.findViewById(R.id.etxtNroCelular);
        etxtNroDocumento=(EditText) root.findViewById(R.id.etxtNroDoc);
        spnNacionalidad=(Spinner) root.findViewById(R.id.spnNacionalidad);
        spnTipoDocumento=(Spinner) root.findViewById(R.id.spnTipoDocumento);
        btnContinuar=(Button) root.findViewById(R.id.btnContinuar);
        btnAtras=(Button) root.findViewById(R.id.btnAtras);
    }

    void listener(){
     //   btnContinuar.setOnClickListener(fragemt: this);
      //  btnAtras.setOnClickListener(this);
    }
}
