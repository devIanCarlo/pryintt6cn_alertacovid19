package edu.cibertec.alertacovid19.UI;

import androidx.appcompat.app.AppCompatActivity;
import edu.cibertec.alertacovid19.Entidad.Ciudadanos;
import edu.cibertec.alertacovid19.Entidad.Triaje;
import edu.cibertec.alertacovid19.R;
import edu.cibertec.alertacovid19.Retrofit.Covid19Client;
import edu.cibertec.alertacovid19.Retrofit.Covid19Service;
import edu.cibertec.alertacovid19.TriajeActivity;
import retrofit2.*;

import android.content.Intent;
import android.util.Log;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

import java.util.function.ToDoubleBiFunction;

public class RegistroCiudadano2 extends AppCompatActivity implements View.OnClickListener {
    EditText etxtNombres, etxtApellidos, etxtNroCel,etxtNroDocumento;
    Spinner spnSexo,spnNacionalidad,spnTipoDocumento;
    Button btnContinuar,btnAtras;

    Covid19Service covid19Service;
    Covid19Client covid19Client;
    Ciudadanos cius=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_ciudadano2);
        covid19Client= Covid19Client.getInstance();
        covid19Service=covid19Client.getCovid19Service();
        findView();
        listener();
    }

    void findView(){
        etxtNombres=(EditText) findViewById(R.id.etxtNombres);
        //etxtApellidos=(EditText) findViewById(R.id.etxtApellidos);
        etxtNroCel=(EditText) findViewById(R.id.etxtNroCelular);
        etxtNroDocumento=(EditText) findViewById(R.id.etxtNroDoc);
        spnNacionalidad=(Spinner) findViewById(R.id.spnNacionalidad);
        spnTipoDocumento=(Spinner) findViewById(R.id.spnTipoDocumento);
        btnContinuar=(Button) findViewById(R.id.btnContinuar);
        btnAtras= findViewById(R.id.btnAtras);
    }

    void listener(){
        btnContinuar.setOnClickListener(this);
        btnAtras.setOnClickListener(this);
    }

    @Override
    public void onClick(View v){
        if(v==btnContinuar)
        {
            Ciudadanos ciu=new Ciudadanos();
            ciu.setNombres(etxtNombres.getText().toString());
            ciu.setCelular(etxtNroCel.getText().toString());
            ciu.setNumeroDocumento(etxtNroDocumento.getText().toString());
            ciu.setNacionalidad(spnNacionalidad.getSelectedItem().toString());
            ciu.setTipoDocumento(spnTipoDocumento.getSelectedItem().toString());

            Call<Ciudadanos> call=covid19Service.doRegistrarCiudadano(ciu);
            call.enqueue(new Callback<Ciudadanos>() {
                @Override
                public void onResponse(Call<Ciudadanos> call, Response<Ciudadanos> response) {

                    if(response.isSuccessful()){
                        cius=response.body();
                        // goDoRegistrarTriaje();
                        //Log.i("GRABO:" , "SE GRABO");
                        Toast.makeText(getApplicationContext(), "Se grabo", Toast.LENGTH_SHORT).show();

                        //codCiudadano=response.body().getIdCiudadanos().toString();

                        Intent i = new Intent(getApplicationContext(), TriajeActivity.class);
                        Bundle param= new Bundle();

                        param.putInt("id",cius.getIdCiudadanos());
                        param.putString("nombre",cius.getNombres());
                        param.putString("nacionalidad",cius.getNacionalidad());
                        param.putString("nroCelular",cius.getCelular());
                        param.putString("tipoDoc",cius.getTipoDocumento());
                        param.putString("nroDoc",cius.getNumeroDocumento());

                        i.putExtras(param);
                        startActivity(i);

                    }
                    else {
                        //TODO: enviar mensaje Toast  no se grabo
                    }

                }

                @Override
                public void onFailure(Call<Ciudadanos> call, Throwable t) {
                    Log.i("failure1:" , t.getMessage());
                }
            });






        }

    }












    }

